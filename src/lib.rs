use crossbeam::thread;
use std::fs::File;
use std::sync::mpsc;
use std::time::Instant;
use threadpool::ThreadPool;

mod channel;
use channel::Channel;

mod entry;
use entry::Entry;

/// Enum for the packing algorithm to use
#[derive(Copy, Clone)]
pub enum PackingMethod {
    /// Sorts the images by height then arranges them into rows; faster than most other algorithms
    Rows,
    /// Splits the image each image. Slower than Rows, but saves more space
    Splits,
}

/// Represents a texture atlas
///
/// # Examples
///
/// ```
/// use tatlap::{Atlas, PackingMethod};
/// let mut a = Atlas::new();
/// a.add_image(String::from("sprite.png"));
/// // ...
/// a.pack(1024, 1024, PackingMethod::Splits);
/// a.remove_duplicates();
/// a.save("spritesheet");
/// ```
pub struct Atlas {
    /// This is where the images are stored, sorted by bytes per pixel
    channels: [Channel; 4],
    /// A threadpool for loading images concurrently
    pool: ThreadPool,
    /// The number of images loaded but not yet added to `channels`
    num_pending: usize,
    /// Sender to be cloned
    tx: mpsc::Sender<Entry>,
    /// Reciver to get images from.
    rx: mpsc::Receiver<Entry>,
}

impl Atlas {
    /// Creates a new atlas instance
    pub fn new() -> Self {
        let (tx, rx) = mpsc::channel();
        Atlas {
            channels: [
                Channel::new(1),
                Channel::new(2),
                Channel::new(3),
                Channel::new(4),
            ],
            pool: ThreadPool::new(num_cpus::get()),
            num_pending: 0,
            tx,
            rx,
        }
    }

    /// Packs an atlas into images (one image per channel)
    ///
    ///
    /// # Arguments
    ///
    /// * `max_width` - The maximum width of the resulting images
    /// * `max_height` - The maximum height of the resulting images
    /// * `method` - The packing algorithm to use
    ///
    /// # Examples
    /// ```
    /// # use tatlap::{Atlas, PackingMethod};
    /// # let mut atlas = Atlas::new();
    /// atlas.pack(1024, 1024, PackingMethod::Splits);
    /// ```
    pub fn pack(&mut self, max_width: u32, max_height: u32, method: PackingMethod) {
        self.join();
        thread::scope(|s| {
            for (i, c) in self.channels.iter_mut().enumerate() {
                c.w = 0;
                c.h = 0;
                c.num_pixels = 0;
                c.num_packed = 0;

                if c.entries.len() == 0 {
                    continue;
                }

                s.spawn(move |_| {
                    let now = Instant::now();
                    match method {
                        PackingMethod::Rows => c.pack_rows(max_width, max_height),
                        PackingMethod::Splits => c.pack_splits(max_width, max_height),
                    };
                    println!(
                        "\n\
                    Status for {}:\n\
                    Sprites packed: {}\n\
                    Sprites that didn't fit: {}\n\
                    Pixels used: {}\n\
                    Total pixels {}\n\
                    Output dimensions: {}x{}\n\
                    % used space: {}\n\
                    Duration: {:.2?}",
                        i + 1,
                        c.num_packed,
                        c.entries.len() - c.num_packed as usize,
                        c.num_pixels,
                        c.w * c.h,
                        c.w,
                        c.h,
                        c.num_pixels as f32 / (c.w * c.h) as f32 * 100.0,
                        now.elapsed(),
                    );
                });
            }
        })
        .unwrap();
    }

    /// Saves the atlas to disk.
    /// Will create these files:
    ///
    /// * `name.txt` - text file containing with one line per image entry containing the bytes per
    /// pixel, trimmed x, trimmed y, trimmed width, trimmed height, x location, y location, original
    /// width, and original height of the image.  
    /// * `name.bin` - same as `name.txt`, but in binary form with values encoded in little-endian
    /// 16-bit unsigned integers.  
    /// * `nameX.png` - the resulting packed png image, where `X` is the number of channels in the
    /// image.
    ///
    /// # Arguments
    /// * `name` - The name to prefix the output files with
    pub fn save(&mut self, name: &str) -> std::io::Result<()> {
        self.join();
        let now = Instant::now();

        thread::scope(|s| {
            for c in &self.channels {
                s.spawn(move |_| c.save_image(name).unwrap());
            }
        })
        .unwrap();

        let get = |i| {
            self.channels
                .iter()
                .flat_map(|c| c.entries.iter())
                .map(|e| {
                    e.users
                        .iter()
                        .map(|u| (e, u))
                        .collect::<Vec<(&Entry, &entry::EntryUser)>>()
                })
                .flatten()
                .find(|t| t.1.id == i)
        };

        println!("\nSaved {} in {:?}", name, now.elapsed());

        let mut txt = File::create(format!("{}.txt", name))?;
        let mut bin = File::create(format!("{}.bin", name))?;

        // TODO Maybe get a smarter way of doing this...
        for i in 0..self.total_entries() {
            let (e, u) = get(i).expect("Couldn't find image with id {}!");
            e.save_bin(&mut bin, u)?;
            e.save_txt(&mut txt, u)?;
        }
        Ok(())
    }

    /// Remove duplicat images  
    /// This still needs to be tweaked
    pub fn remove_duplicates(&mut self) {
        self.join();
        thread::scope(|s| {
            for c in &mut self.channels {
                s.spawn(move |_| c.remove_duplicates());
            }
        })
        .unwrap();
    }

    fn get_new_id(&mut self) -> usize {
        let id = self.total_entries();
        self.num_pending += 1;
        id
    }

    /// Return the total number of entries in this atlas
    pub fn total_entries(&self) -> usize {
        self.channels.iter().fold(self.num_pending, |a, c| {
            a + c.entries.iter().fold(0, |a, t| a + t.users.len())
        })
    }

    /// Add an entry to the correct channel
    fn add_entry(&mut self, e: Entry) {
        self.channels[e.image.color().bytes_per_pixel() as usize - 1].add_entry(e);
    }

    /// Wait for all threads to finish
    fn join(&mut self) {
        for _ in 0..self.num_pending {
            let e = self.rx.recv().unwrap();
            self.add_entry(e);
        }
        self.num_pending = 0;
        self.pool.join();
    }

    /// Queues the image at `filename` to be added to the atlas
    pub fn add_image(&mut self, filename: String) {
        let i = self.get_new_id();

        let tx = self.tx.clone();

        self.pool.execute(move || {
            let e = Entry::new(&filename, i);
            tx.send(e).unwrap();
        });
    }

    /// Loads an atlas called `name` such that there is a `name.bin` and `name1.png` to `name4.png`
    /// and adds them to the current atlas
    pub fn add_atlas(&mut self, name: &str) {
        let mut f = File::open(format!("{}.bin", name)).unwrap();

        let mut images = [None, None, None, None];

        for i in 0..4 {
            images[i] = image::open(format!("{}{}.png", name, i + 1)).ok();
        }

        let mut i = self.total_entries();
        loop {
            match Entry::from_atlas_bin(&images, &mut f, i) {
                Some(e) => {
                    self.add_entry(e);
                    i += 1;
                }
                None => break,
            }
        }
    }
}
