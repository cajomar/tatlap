use image::GenericImageView;

fn iterv<I1, I2>(img: &image::DynamicImage, rx: I1, ry: I2) -> (u32, u32)
where
    I1: Iterator<Item = u32>,
    I1: Clone,
    I2: Iterator<Item = u32>,
    I2: Clone,
{
    let mut dx: u32 = 0;
    let mut dy: u32 = 0;
    let i = img.color().bytes_per_pixel() as usize - 1;
    for y in ry {
        dy = y;
        for x in rx.clone() {
            dx = x;
            let p = img.get_pixel(x, y);
            if p.0[i] > 0 {
                return (x, y);
            }
        }
    }
    (dx, dy)
}

fn iterh<I1, I2>(img: &image::DynamicImage, rx: I1, ry: I2) -> (u32, u32)
where
    I1: Iterator<Item = u32> + Clone,
    I2: Iterator<Item = u32> + Clone,
{
    let mut dx: u32 = 0;
    let mut dy: u32 = 0;

    let i = img.color().bytes_per_pixel() as usize - 1;

    for x in rx {
        dx = x;
        for y in ry.clone() {
            dy = y;
            let p = img.get_pixel(x, y);

            if p.0[i] > 0 {
                return (x, y);
            }
        }
    }
    (dx, dy)
}

pub fn get_bounds(img: &image::DynamicImage) -> (u32, u32, u32, u32) {
    let (w, h) = img.dimensions();
    let (maxl, t) = iterv(img, 0..w, 0..h);
    let (l, minb) = iterh(img, 0..maxl, (t + 1..h).rev());
    let (minr, b) = iterv(img, (l + 1..w).rev(), (minb..h).rev());
    let (r, _) = iterh(img, (minr..w).rev(), (t + 1..b).rev());

    /*
    let (_, t) = iter(img, 0..w, 0..h, 0, 0, true);
    let (l, _) = iter(img, 0..w, 0..h, 0, 0, false);
    let (_, b) = iter(img, 0..w, (0..h).rev(), 0, 0, true);
    let (r, _) = iter(img, (0..w).rev(), 0..h, 0, 0, false);
    */
    (l, t, r - l + 1, b - t + 1)
}
