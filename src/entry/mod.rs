use std::fs::File;
use std::io::prelude::Read;
use std::io::prelude::Write;

use image::GenericImageView;

mod bounds;

// TODO get a better name
pub struct EntryUser {
    pub id: usize,
    pub l: u32,
    pub t: u32,
    pub original_w: u32,
    pub original_h: u32,
}

pub struct EntryRect {
    pub x: u32,
    pub y: u32,
}

pub struct Entry {
    pub image: image::DynamicImage,
    pub users: Vec<EntryUser>,
    pub packed: Option<EntryRect>,
}

impl Entry {
    pub fn new(filename: &str, id: usize) -> Self {
        match Self::load(filename, id) {
            Ok(e) => e,
            Err(e) => {
                eprintln!("Failed to open {}: {}", filename, e);
                Self::dummy(id)
            }
        }
    }

    fn load(filename: &str, id: usize) -> Result<Self, image::ImageError> {
        let img = image::open(filename)?;

        let (l, t, w, h) = match img.color().bytes_per_pixel() {
            3 => (0, 0, img.width(), img.height()),
            _ => bounds::get_bounds(&img),
        };

        if w == 0 || h == 0 {
            panic!("Image is completely transparent!");
        }

        let i = img.crop_imm(l, t, w, h);

        Ok(Entry {
            image: i,
            users: vec![EntryUser {
                id,
                l,
                t,
                original_w: img.width(),
                original_h: img.height(),
            }],
            packed: None,
        })
    }

    pub fn dummy(id: usize) -> Self {
        Self {
            image: image::DynamicImage::new_rgba8(1, 1),
            users: vec![EntryUser {
                id,
                l: 0,
                t: 0,
                original_w: 1,
                original_h: 1,
            }],
            packed: None,
        }
    }

    pub fn from_atlas_bin(
        images: &[Option<image::DynamicImage>; 4],
        file: &mut File,
        id: usize,
    ) -> Option<Self> {
        println!("loading {}", id);

        let mut bpp = [0u8; 1];
        if file.read(&mut bpp).unwrap() < 1 {
            return None;
        }
        let bpp = bpp[0];

        let mut buf = [0u16; 8];
        unsafe {
            if file.read(buf.align_to_mut().1).unwrap() < 8 * 2 {
                return None;
            }
        };

        if bpp == 0 {
            return Some(Self::dummy(id));
        }

        println!("bpp is {}", bpp);
        assert!(bpp <= 4);

        match &images[bpp as usize - 1] {
            Some(img) => Some(Self {
                image: img.crop_imm(buf[0] as u32, buf[1] as u32, buf[2] as u32, buf[3] as u32),
                users: vec![EntryUser {
                    id,
                    l: buf[4] as u32,
                    t: buf[4] as u32,
                    original_w: buf[4] as u32,
                    original_h: buf[4] as u32,
                }],
                packed: None,
            }),
            None => Some(Self::dummy(id)),
        }
    }

    pub fn w(&self) -> u32 {
        self.image.width()
    }

    pub fn h(&self) -> u32 {
        self.image.height()
    }

    fn get_stuff_to_save(&self, user: &EntryUser) -> [u16; 8] {
        match &self.packed {
            Some(r) => [
                r.x as u16,
                r.y as u16,
                self.w() as u16,
                self.h() as u16,
                user.l as u16,
                user.t as u16,
                user.original_w as u16,
                user.original_h as u16,
            ],
            None => [
                0,
                0,
                0,
                0,
                user.l as u16,
                user.t as u16,
                user.original_w as u16,
                user.original_h as u16,
            ],
        }
    }

    pub fn save_txt(&self, file: &mut File, user: &EntryUser) -> std::io::Result<()> {
        let s = self.get_stuff_to_save(user);
        write!(
            file,
            "{} {} {} {} {} {} {} {} {}\n",
            self.image.color().bytes_per_pixel(),
            s[0],
            s[1],
            s[2],
            s[3],
            s[4],
            s[5],
            s[6],
            s[7],
        )?;
        Ok(())
    }

    pub fn save_bin(&self, file: &mut File, user: &EntryUser) -> std::io::Result<()> {
        file.write(&[self.image.color().bytes_per_pixel()])?;
        for t in self.get_stuff_to_save(user) {
            file.write(&t.to_le_bytes())?;
        }
        Ok(())
    }
}
