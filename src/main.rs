use std::fs::File;
use std::io::{self, BufRead};
use std::process;
use tatlap::{Atlas, PackingMethod};

fn fail(msg: &str) -> String {
    eprintln!("{}", msg);
    process::exit(1);
}

struct CommandOption<'a> {
    long: &'a str,
    short: char,
    help: &'a str,
    arg: bool,
    command: fn(&mut State) -> (),
}

struct State<'a> {
    atlas: Atlas,
    out: String,
    arg: Option<String>,
    options: &'a [CommandOption<'a>],
    dedup: bool,
}

fn co<'a>(l: &'a str, s: char, h: &'a str, a: bool, c: fn(&mut State) -> ()) -> CommandOption<'a> {
    CommandOption {
        long: l,
        help: h,
        short: s,
        arg: a,
        command: c,
    }
}

fn main() {
    let options = [
        co("dedup", 'd', "Try to remove duplicate images", false, |s| {
            s.dedup = true;
        }),
        co("out", 'o', "Specify the output name", true, |s| {
            s.out = s.arg.take().unwrap()
        }),
        co("stdin", 'i', "Read image names from stdin", false, |s| {
            for f in io::stdin().lock().lines().filter_map(|x| x.ok()) {
                s.atlas.add_image(f);
            }
        }),
        co("file", 'f', "Read image names from a file", true, |s| {
            let filename = s.arg.take().unwrap();
            let file = File::open(&filename);
            if let Err(e) = &file {
                fail(&format!("Error: Failed to open file {}: {}", filename, e));
            }
            io::BufReader::new(file.unwrap())
                .lines()
                .for_each(|l| match l {
                    Ok(f) => s.atlas.add_image(f),
                    Err(e) => eprintln!("{}", e),
                })
        }),
        co("atlas", 'a', "Load an atlas made by tatlap", true, |s| {
            s.atlas.add_atlas(&s.arg.take().unwrap())
        }),
        co("help", 'h', "Print this message and exit", false, |s| {
            println!("Usage: tatlap [OPTIONS] [IMAGES]\n");
            for o in s.options {
                println!(
                    "    -{}, --{}{}\n            {}\n",
                    o.short,
                    o.long,
                    if o.arg { " ARG" } else { "" },
                    o.help
                );
            }
            process::exit(0);
        }),
    ];

    let mut state = State {
        atlas: Atlas::new(),
        arg: None,
        out: String::from("out"),
        options: &options,
        dedup: false,
    };

    let mut args = std::env::args();
    args.next();

    let mut parsing = true;

    while let Some(a) = args.next() {
        if parsing && a.starts_with("--") {
            if a.len() == 2 {
                parsing = false;
                continue;
            }
            match options.iter().find(|o| &a[2..] == o.long) {
                Some(o) => {
                    if o.arg {
                        state.arg = args.next();
                        if state.arg.is_none() {
                            fail("Error: expected argument");
                        }
                    }
                    (o.command)(&mut state);
                }
                None => eprintln!("Invalid option {}. Try --help", a),
            }
        } else if parsing && a.starts_with("-") {
            for (i, c) in a[1..].char_indices() {
                match options.iter().find(|o| c == o.short) {
                    Some(o) => {
                        if o.arg {
                            if i < a.len() - 2 {
                                // Still more characters; use them as the argument
                                state.arg = Some(a[i + 2..].to_string())
                            } else {
                                // This is the last character in the strig
                                // Use the next argument
                                state.arg = args.next();
                                if state.arg.is_none() {
                                    fail("Error: expected argument");
                                }
                            };
                        }
                        (o.command)(&mut state);
                        if o.arg {
                            // We used the rest of string as the argument
                            break;
                        }
                    }
                    None => eprintln!("Invalid flag -{}. Try --help", c),
                };
            }
        } else {
            state.atlas.add_image(a);
        }
    }

    if state.atlas.total_entries() > 0 {
        if state.dedup {
            state.atlas.remove_duplicates();
        }
        state.atlas.pack(0, 0, PackingMethod::Splits);
        state.atlas.save(&state.out).unwrap();
    } else {
        println!("No images given");
    }
}
